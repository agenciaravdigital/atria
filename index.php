<?php ob_start("ob_gzhandler"); ?>

<!DOCTYPE html>
<?php
$smart_crm = '';

if ( isset($_GET['smart_crm']) && $_GET['smart_crm'] != '' ) {
	$smart_crm = $_GET['smart_crm'];
} else {

	$redirect = 'Location: http://' . $_SERVER['HTTP_HOST'] . '?smart_crm=178';

	if ( isset($_SERVER['HTTP_REFERER']) ) {
		if ( strpos($_SERVER['HTTP_REFERER'], 'google.com') !== false) {
			$redirect = 'Location: http://' . $_SERVER['HTTP_HOST'] . '?smart_crm=182';
		}
	} else if ( strpos($_SERVER['HTTP_HOST'], 'agenciagtc') !== false ) {
		$redirect = 'Location: http://' . $_SERVER['HTTP_HOST'] . '/atria?smart_crm=182';
	}

	header($redirect);
}

$system = ( strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false ) ? 'windows' : 'mac';

?>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" lang="pt-BR"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="pt-BR"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="pt-BR"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="pt-BR"><![endif]-->
<!--[if(gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="pt-br">
<!--<![endif]-->
<head>
	<!-- Meta-Tags-->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Título -->
	<title>Atria Alphaville</title>
	<!-- Google Tags -->
	<meta name="author" content="Agência GTC - http://gtc.ag/">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<!-- Styles -->
	
	<link rel="stylesheet" href="assets/css/main.css" type="text/css" media="all">
	<!--
	<link rel="stylesheet" href="assets/css/main.min.css" type="text/css" media="all">
	-->
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">
	<!-- IE Modenizr -->
	<!--[if IE]><script src="assets/js/modenizr.js"></script><![endif]-->
	<link rel="canonical" href="http://atriampd.com.br">
	<script>
		/** Google Tag Manager  **/
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TT935M9');
		/** End Google Tag Manager  **/
  
  		/** Google Analytics  **/
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-77730064-3', 'auto');
		ga('send', 'pageview');

</script>
</head>

<body class="<?=$system?>">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TT935M9" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<header>
		<nav class="navbar">
				
			<div class="container-small header-line">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
					<a class="navbar-brand logoHeader page-scroll" href="#">
						<img src="assets/images/logo.jpg" alt="Atria Alphaville">
					</a>
				</div>

				<div class="collapse navbar-collapse" id="menu">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#gallery" class="page-scroll">Galeria</a></li>
						<li><a href="#video" class="page-scroll">Vídeo</a></li>
						<li><a href="#plan" class="page-scroll">Plantas</a></li>
						<li><a href="#services" class="page-scroll">Alphaville</a></li>
						<li><a href="#location" class="page-scroll">Localização</a></li>
						<li><a href="#differential" class="page-scroll">Diferenciais</a></li>
						<li><a href="#idealizers" class="page-scroll">projeto</a></li>
					</ul>
				</div>
				<div class="col-sm-3 col-md-2 text-header pull-right">
					<span>(11) 3522-6100</span>
				</div>
			</div>
		</nav>
	</header>
	
	<section class="banner">
		<div class="container-small">
			<h2>Alphaville aos seus pés, em um projeto único</h2>
		</div>
		
		<div class="icons-contact">
			<ul class="contato">
		    	<li>
		            <a class="btn btn-default button-chat button-interection" href="javascript:sendSmartStaffChat('mpd')">
		                <i class="fa fa-comments-o"></i>
		                <div class="text">Corretor Online<br>
		                    <span style="font-size:12px">Atendimento de segunda à domingo das 09h às 23h</span>
		                </div>
		            </a>
		         </li>
		    	<li>
		            <a class="btn btn-default button-call button-interection" href="#" data-toggle="modal" data-target="#formModal">
		                <i class="fa fa-phone"></i>		                
		                <span class="text">Ligamos para você </span>
		            </a>
		         </li>
		    	<li>
		            <a class="btn btn-default button-msg button-interection" href="#" data-toggle="modal" data-target="#formModal">
		             	<i class="fa fa-envelope-o"></i>
		                <span class="text">Envie uma mensagem</span>
		            </a>
		        </li>
		    </ul>
		</div>
	</section>

	<section class="description" id="description">
		<div class="container">
			<h1><span class="principal">Atria</span> <span class="full">Alphaville</span></h1>
			<div class="legend">
				<span>–</span> 
				<span>APARTAMENTOS DE LUXO</span> 
				<span>A PARTIR DE 228M²</span> 
				<span>–</span>
			</div>
			<p class="text1">A MPD e a Helbor trazem para Alphaville um marco de elegância e tradição. Um empreendimento que une o alto padrão construtivo e a imponência arquitetônica em uma localização privilegiada. Projeto impecável, plantas que integram perfeitamente todos os espaços.</p>
			<blockquote>
				<span class="footage"><span>228 e 285 m<sup>2</sup></span> <span class="ocult">|</span> <span>4 Suítes</span></span>
				<span class="text">Torre Única em um terreno de 5.500m<sup>2</sup></span>
			</blockquote>
			<p class="text2">Um lançamento sob medida, exclusivo, em uma das localizações mais desejadas.</p>
		</div>
	</section>

	<section class="gallery">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>Galeria</h2>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="box maxh414 maxw400">
								<img src="assets/images/gallery/image-min-1.jpg" data-num="1" alt="Fachada Diurna" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="row">
								<div class="col-md-12">
									<div class="box maxh192 maxw400">
										<img src="assets/images/gallery/image-min-2.jpg" data-num="2" alt="Portaria" data-toggle="modal" data-target="#photosModal" class="image">
										<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="box maxh192 maxw400">
										<img src="assets/images/gallery/image-min-5.jpg" data-num="5" alt="Playground" data-toggle="modal" data-target="#photosModal" class="image">
										<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-7.jpg" data-num="7" alt="Vaga Box" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-8.jpg" data-num="8" alt="Brinquedoteca" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-9.jpg" data-num="9" alt="Fitness" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-10.jpg" data-num="10" alt="Sala de ginástica" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-3.jpg" data-num="3" alt="Boulevard" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-4.jpg" data-num="4" alt="Vista Lazer" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="box maxh192 maxw400">
								<img src="assets/images/gallery/image-min-6.jpg" data-num="6" alt="Hall Social" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="box maxh414">
								<img src="assets/images/gallery/image-min-11.jpg" data-num="11" alt="Salão de Jogos" data-toggle="modal" data-target="#photosModal" class="image">
								<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="more-gallery">
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<div class="box maxh414 maxw400">
							<img src="assets/images/gallery/image-min-12.jpg" data-num="12" alt="Suíte master (apto 228²)" data-toggle="modal" data-target="#photosModal" class="image">
							<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
						</div>
					</div>
					<div class="col-md-9 col-sm-12">
						<div class="box maxh414 maxw400">
							<img src="assets/images/gallery/image-min-13.jpg" data-num="13" alt="Living (apto 228m²)" data-toggle="modal" data-target="#photosModal" class="image">
							<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="box maxh414 maxw400">
							<img src="assets/images/gallery/image-min-14.jpg" data-num="14" alt="Zoom da fachada" data-toggle="modal" data-target="#photosModal" class="image">
							<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-16.jpg" data-num="16" alt="Piscina descoberta" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-17.jpg" data-num="17" alt="Salão de festas gourmet" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-18.jpg" data-num="18" alt="Salão de festas gourmet" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-19.jpg" data-num="19" alt="Quadra de squash" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-20.jpg" data-num="20" alt="Terraço (apto 228m²)" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-21.jpg" data-num="21" alt="Pet place" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-22.jpg" data-num="22" alt="Car Wash Box" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="box maxh192 maxw400">
									<img src="assets/images/gallery/image-min-23.jpg" data-num="23" alt="Pet care" data-toggle="modal" data-target="#photosModal" class="image">
									<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="box maxh414 maxw400">
							<img src="assets/images/gallery/image-min-15.jpg" data-num="15" alt="Voo duplex" data-toggle="modal" data-target="#photosModal" class="image">
							<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="box maxh414">
							<img src="assets/images/gallery/image-min-24.jpg" data-num="24" alt="Piscinas cobertas climatizadas" data-toggle="modal" data-target="#photosModal" class="image">
							<div class="icon-zoom" data-toggle="modal" data-target="#photosModal"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
						</div>
					</div>
				</div>
			</div>
			<div class="more-gallery-link">
				<div class="row">
					<div class="col-md-12">
						<img src="assets/images/more-gallery.png" alt="Veja mais fotos do empreendimento" class="plus">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="video">
		<div class="container">
			<div class="col-md-12">
				<div class="title">
					<h2>Vídeo</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div style="position:relative;height:0;padding-bottom:56.25%">
						<iframe src="https://www.youtube.com/embed/SnHyloWDhzo?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen>
						</iframe>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="faca-tour">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<button class="button" data-toggle="modal" data-target="#tourModal">
						TOUR VIRTUAL 360° | APTO DECORADO DE 285M²
					</button>
				</div>
			</div>
		</div>
	</section>

	<section class="plan">
		<div class="container">
			<div class="title">
				<h2>Plantas</h2>
				<div class="legend"><span>Unidades de 228m<sup>2</sup>, 285m<sup>2</sup>, 379m<sup>2</sup> e 498m<sup>2</sup></span></div>
			</div>
			<div class="plans-image">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-1.jpg" alt="379m² (Final 3)" data-num="1" class="image"></a>
								</div>
								<span class="text">
									379m²
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-2.jpg" alt="379m² (Final 2)" data-num="2" class="image"></a>
								</div>
								<span class="text">
									379m²
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-3.jpg" alt="498m²" data-num="3" class="image"></a>
								</div>
								<span class="text">
									498m²
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-4.jpg" alt="379m²" data-num="4" class="image"></a>
								</div>
								<span class="text">
									379m²
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-5.jpg" alt="379m²" data-num="5" class="image"></a>
								</div>
								<span class="text">
									379m²
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="box">
							<div class="box-content" data-toggle="modal" data-target="#photosModal">
								<div class="box-img box-img-plan">
									<a href="#"><img src="assets/images/gallery/plan-min-6.jpg" alt="379m²" data-num="6" class="image"></a>
								</div>
								<span class="text">
									379m²
								</span>
							</div>
						</div>
					</div>
					<div class="more-plans">
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-7.jpg" alt="379m²" data-num="7" class="image"></a>
									</div>
									<span class="text">
										379m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-8.jpg" alt="498m²" data-num="8" class="image"></a>
									</div>
									<span class="text">
										498m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-9.jpg" alt="498m²" data-num="9" class="image"></a>
									</div>
									<span class="text">
										498m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-16.jpg" alt="285m²" data-num="16" class="image"></a>
									</div>
									<span class="text">
										285m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-17.jpg" alt="285m²" data-num="17" class="image"></a>
									</div>
									<span class="text">
										285m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-12.jpg" alt="228m² (Final 2)" data-num="12" class="image"></a>
									</div>
									<span class="text">
										228m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-13.jpg" alt="228m² (Final 3)" data-num="13" class="image"></a>
									</div>
									<span class="text">
										228m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-14.jpg" alt="228m² living ampliado (Final 2)" data-num="14" class="image"></a>
									</div>
									<span class="text">
										228m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-15.jpg" alt="228m² living ampliado (Final 3)" data-num="15" class="image"></a>
									</div>
									<span class="text">
										228m²
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-md-offset-2">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-10.jpg" alt="Implantação Life Style" data-num="10" class="image"></a>
									</div>
									<span class="text">
										Implantação Life Style
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="box">
								<div class="box-content" data-toggle="modal" data-target="#photosModal">
									<div class="box-img box-img-plan">
										<a href="#"><img src="assets/images/gallery/plan-min-11.jpg" alt="Implantação Boulevard (fundo branco)" data-num="11" class="image"></a>
									</div>
									<span class="text">
										Implantação Boulevard
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="more-plans-link">
					<div class="row">
						<div class="col-md-12">
							<img src="assets/images/more-gallery.png" alt="Veja mais fotos do empreendimento" class="plus">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="services">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>Essencialmente Alphaville</h2>
						<div class="legend"><span>Gastronomia contemporânea, grifes, avenidas<br> largas e arborizadas. Tudo ao seu redor</span></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/colegios-e-universidades.jpg" alt="Escola Internacional de Alphaville,Escola Morumbi,Escola Castanheiras,Snowflake School,Damasio Educacional,Ursinho Branco,Maple Bear Canadian School Alphaville,Saint Nicholas,Mackenzie,PUC,FGV,Unip,Unip Objetivo,FATEC,FIEB,SENAI,CEL-LEP">
						<span class="legend">Colégios e Universidades</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/supermercados.jpg" alt="Pão de Açúcar,Sam’s Club,St Marche,Wal Mart,Carrefour,Mambo">
						<span class="legend">Supermercados</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/lazer.jpg" alt="C&C,Telha Norte,Leroy Merlin,Decathlon,Sodimac,Alphaville Tênis Clube">
						<span class="legend">Lazer</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/hospitais-e-clinicas.jpg" alt="Albert Einstein,Fleury,Amil,Delboni Auriemo,Lavoisier">
						<span class="legend">Hospitais e Clínicas</span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/academias.jpg" alt="Academia 24h,Target Fitclub,Body Tech,Bio Ritmo,CrossFit Barueri">
						<span class="legend">Academias</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/shoppings.jpg" alt="Shopping Iguatemi Alphaville,Shopping Tamboré,Alpha Shopping,Shopping Flamingo,Alpha Square Mall">
						<span class="legend">Shoppings</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/padarias.jpg" alt="A Quinta do Conde,La Ville,Fran’s Café">
						<span class="legend">Padarias</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-services" data-toggle="modal" data-target="#servicesModal">
						<img src="assets/images/restaurantes.jpg" alt="El Uruguayo,Grand Cru,Pobre Juan,Ville du Vin,L’Entrecôte de Paris,Maria João,Novilho de Prata,PF Chang’s,General Prime Burguer,Maremont,Almanara,América,Bar do Alemão,Outback,Pizza Hut,Osaka">
						<span class="legend">Restaurantes</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="location">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>Localização</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="map"></div>
	</section>

 	<section class="differential">
 		<div class="container">
 			<div class="row">
 				<div class="col-md-12">
					<div class="title">
						<h2>Diferenciais</h2>
					</div>
				</div>
 			</div>
 		</div>
		<div class="container-small">
			<div class="differential-box text-right">
				<div class="row">
					<div class="box-row">
						<div class="box-img left">
							<div class="img">
								<img src="assets/images/conforto.jpg" alt="">
							</div>
						</div>
						<div class="box-text right">
							<div class="text">
								<h3>Conforto</h3>
								<div class="legend">
									<strong>Conforto em ambientes</strong> exclusivos e autênticos.
								</div>
								<div class="content">
									<p>Um ambiente essencial com espaço para banho e tosa. Instalações perfeitas que atendem com segurança e conforto a necessidade do seu pet.</p>
									<p>Espaço especialmente planejado para os cuidados com seu carro. Seu motorista, conta também com uma sala de apoio para descanso com conforto. Conforto em ambientes exclusivos e autênticos</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="differential-box text-left">
				<div class="row">
					<div class="box-row">
						<div class="box-text left">
							<div class="text">
								<h3>Segurança</h3>
								<div class="legend">
									Controle absoluto. Tecnologia de última geração garantindo privacidade e segurança.
								</div>
								<div class="content">
									<p>Fechadura<br />Biometrica</p>
									<p>Biometria nos<br />elevadores</p>
									<p>Central de alerta<br />e monitoramento</p>
									<p>Portaria Blindada</p>
									<p>Pulmão de acesso</p>
									<p>Gerador para resgate<br />dos elevadores</p>
									<p>Câmera de segurança</p>
								</div>
							</div>
						</div>
						<div class="box-img right">
							<div class="img">
								<img src="assets/images/seguranca.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="differential-box text-right">
				<div class="row">
					<div class="box-row">
						<div class="box-img left">
							<div class="img">
								<img src="assets/images/sustentabilidade.jpg" alt="">
							</div>
						</div>
						<div class="box-text right">
							<div class="text">
								<h3>Sustentabilidade</h3>
								<div class="legend">
									Vida inteligente. Consumo racional através de recursos que priorizam o bem-estar e o meio ambiente.
								</div>
								<div class="content">
									<p>Sistema automático de<br />irrigação dos jardins<br />com agua de reuso</p>
									<p>Central de recepção de lixo<br />com espaço para armazenamento<br />de lixo reciclável proveniente<br />de coleta seletiva</p>
									<p>Bacias sanitárias com<br />acionamento duplo</p>
									<p>Medição de água<br />individualizada</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="idealizers">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>Projeto</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="box-perfil box-perfil-1">
						<span class="img-perfil"><img src="assets/images/person-2.jpg" alt="eneditto Abbud - Paisagismo"></span>
						<p>O projeto de paisagismo do Atria foi inspirado em jardins exuberantes de cores, sabores e texturas. Os ambientes sociais ao ar livre se conectam aos espaços internos, garantindo momentos de lazer e relaxamento para toda a família.</p>
						<span class="name">Beneditto Abbud</span>
						<span class="role">Paisagismo</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-perfil box-perfil-2">
						<span class="img-perfil"><img src="assets/images/person-1.jpg" alt="LE Arquitetura - Arquitetura (conceito)"></span>
						<p>Locado em um dos pontos mais altos de toda região, criamos um projeto para que o usuário tenha como companhia o verde do local e as vistas sem fim. Desde sua chegada por uma via exclusiva, aos ambientes amplos, sempre com muita luz, lazer incrível e a fachada de linhas retas e elegantes, o Atria é um projeto único que se tornara referência.</p>
						<span class="name">LE Arquitetura</span>
						<span class="role">Arquitetura (conceito)</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-perfil box-perfil-3">
						<span class="img-perfil"><img src="assets/images/person-4.jpg" alt="Quitete e Faria - Decoração"></span>
						<p>A decoração das áreas comuns do Atria, foi desenvolvida para trazer beleza, sofisticação, conforto  e funcionalidade aos moradores e seus convidados, tonando-se uma extensão de suas residências.</p>
						<span class="name">Quitete e Faria</span>
						<span class="role">Decoração</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="box-perfil box-perfil-4">
						<span class="img-perfil"><img src="assets/images/person-3.jpg" alt="Reinaldo Pestana - Arquitetura (desenvolvimento)"></span>
						<p>Como arquiteto é gratificante estar desenvolvendo um empreendimento como o Atria, com uma concepção de alto-padrão, que combinou conforto, qualidade, tecnologia e segurança para oferecer um dos melhores produtos do segmento.</p>
						<span class="name">Reinaldo Pestana</span>
						<span class="role">Arquitetura (desenvolvimento)</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="companies">
		<div class="container">
			<div class="builder">
				<div class="row">
					<div class="col-md-12">
						<div class="title">
							<h2>Parceria que une qualidade construtiva e responsabilidade com os clientes</h2>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row mpd">
							<div class="col-md-12 logo">
								<img src="assets/images/logo-mpd.png" alt="MPD">
							</div>
							<div class="col-md-12 text">
								<p>A MPD atua há 35 anos na construção e incorporação de apartamentos de médio e alto padrão, escritórios, consultórios, shopping centers, escolas, hospitais e indústrias, prezando pela qualidade de acabamento, entrega 100% no prazo e pelo respeito aos seus clientes e colaboradores. Além disso, investe em desenvolvimento tecnológico e boas práticas construtivas, sem deixar de lado a  responsabilidade social e ambiental. Para a MPD, seus colaboradores são seu principal pilar, sendo primordiais para que alcance seus objetivos de negócio. Pela constante valorização de sua equipe, a empresa já foi reconhecida duas vezes com o Prêmio Valor Carreira como uma das Melhores Empresas na Gestão de Pessoas e quatro vezes como uma das Melhores Empresas para Você Trabalhar da revista Você S.A</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row helbor">
							<div class="col-md-12 logo">
								<img src="assets/images/logo-helbor.png" alt="HELBOR">
							</div>
							<div class="col-md-12 text">
								<p>Uma história de determinação e desenvolvimento. Do lançamento de um imóvel na planta até a entrega das chaves de um apartamento ou de uma sala comercial, percorremos um longo caminho inspirado na relação de confiança com nossos clientes e no trabalho de milhares de pessoas. Essa trajetória tem como base o exemplo de coragem e determinação do imigrante Hélio Borenstein, homenageado no próprio nome Helbor. Em 40 anos, desenvolvemos 227 projetos, que somam 6 milhões de m². No total, são mais de 30 mil unidades entregues, entre apartamentos, casas, conjuntos comerciais, unidades hoteleiras e lotes urbanizados.</p>	
								<p>Estamos presentes em 30 cidades brasileiras, em 10 estados e no Distrito Federal. Nesse período, temos contribuído para o desenvolvimento do mercado imobiliário brasileiro, oferecendo excelentes oportunidades de negócio, produtos inovadores e conceitos que aproximam as pessoas e valorizam os espaços. Nossa atuação está focada na relação de transparência estabelecida com nossos clientes, funcionários, parceiros, fornecedores, investidores e amigos, respeitando as particularidades de cada região em que atuamos e cumprindo rigorosamente nossos compromissos.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="container">
			<div class="company">
				<span class="logoAtria"><a href="#" class="btnLogo logoFooter"><img src="assets/images/logo.jpg" alt=""></a></span>
				<span class="text text-footer">ATRIA ALPHAVILLE <span class="hidden-sm hidden-xs">|</span> <span>INFORMAÇÕES E VENDAS</span> <span>- (11) 3522-6100</span></span>
			</div>
		</div>
	</footer>

	<div class="modal fade" id="photosModal" tabindex="-1" role="dialog" aria-labelledby="photosLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="photosLabel">Galeria</h4>
				</div>
				<div class="modal-body">
					<div class="arrows">
						<span class="arrowLeft arrow"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></span>
						<span class="arrowRight arrow"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></span>
					</div>
					<div class="getImage">Carregando...</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formLabel">
		<div class="modal-dialog dialog-ms" role="document">
			<div class="modal-content">
				<form action="http://integracaocrm.smartstaff.com.br/integracaoCrmUnificado.php" method="post">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="formLabel">Formulário de contato</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="url" id="url" value="http://atriampd.com.br?smart_crm=<?=$smart_crm?>">
						<input type="hidden" name="tipo" id="tipo" value="email">
						<input type="hidden" name="empresa" id="empresa" value="MPD">
						<input type="hidden" name="empreendimento" id="empreendimento" value="Hotsite Atria">
						<input type="hidden" name="smart_crm" id="smart_crm" value="<?=$smart_crm?>">
						<div class="group-form">
							<label for="nome">Nome <small>Obrigatório</small></label>
							<input type="text" name="nome" id="nome" class="form-control">
						</div>
						<div class="group-form">
							<label for="email">E-mail</label>
							<input type="text" name="email" id="email" class="form-control">
						</div>
						<div class="group-form">
							<label for="telefone">Telefone</label>
							<input type="text" name="telefone" id="telefone" class="form-control">
						</div>
						<div class="group-form">
							<label for="mensagem">Mensagem</label>
							<textarea name="mensagem" id="mensagem" class="form-control"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-primary btn-valid">Enviar</button>
						<button type="submit" class="btn btn-primary btn-send hide">Enviar</button>
					</div>
					<div class="msg"></div>
				</form>
			</div>
		</div>
	</div>

	<!-- Box modal "Essencialmente Alphaville" -->
	<div class="modal fade" id="servicesModal" tabindex="-1" role="dialog" aria-labelledby="formLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3>Carregando...</h3>
				</div>
				<div class="modal-body">
					<div class="boxItems">Carregando...</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Box message fixed -->
	<div class="box-container-message">
		<div class="icon-toggle _show">
			<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		</div>
		<a href="javascript:sendSmartStaffChat('mpd')" class="button-interection button-chat">
			<div class="box-message">
				<span class="text">ATENDIMENTO<br />ONLINE</span>
				<button>Conversar</button>
			</div>
		</a>
	</div>
	<!-- END Box message fixed -->
	
<!-- Box modal "Tour" -->
<!--
	<div class="modal fade" id="servicesModal" tabindex="-1" role="dialog" aria-labelledby="formLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3>Carregando...</h3>
				</div>
				<div class="modal-body">
					<div class="boxItems">Carregando...</div>
				</div>
			</div>
		</div>
	</div>
	-->
<section class="modal_Tour">
	<div class="modal fade" id="tourModal" tabindex="-1" role="dialog" aria-labelledby="leedLabel"> 
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tourModal"><b>+</b>  TOUR VIRTUAL 360° | APTO DECORADO DE 285M²</h4>
				</div>
				<div class="modal-body">
						<iframe src="http://www.sferica.com.br/temporario/mpd/atria/" width="100%" height="700" frameborder="0" allowfullscreen></iframe>	
				</div>
			</div>
		</div>
	</div>
	<!-- END Box message fixed -->	
</section>
	

	<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" media="all">
	
	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/main.min.js"></script>
	<script src="https://s3-sa-east-1.amazonaws.com/smart-crm/js/smart.crm.dev.min.js"></script>
</body>
</html>