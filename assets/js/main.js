var modalArea 			= '';
var elem 				= '';
var sectionPrincipal	= '';
var subImgName 			= '';
var originToGa			= 'Form';

$(document).ready(function(){

	$(window).resize(function(){
		adjustBoxMessage();
	});

	/**
	 * Gallery
	 */
	$('#photosModal').on('shown.bs.modal', function (event) {
	  	elem 	= $(event.relatedTarget);

	  	if(elem.closest('.gallery').length > 0){
	  		sectionPrincipal 	= $('.gallery');
	  		subImgName 			= 'image';
	  	} else {
	  		sectionPrincipal 	= $('.plan');
	  		subImgName 			= 'plan';
	  	};

	  	modalArea 	= $(this);
	  	loadGallery(elem);
	});

	/**
	 * clique em imagens de referenciais, abrindo modal com descrição de cada
	 */
	$('#servicesModal').on('shown.bs.modal', function (event) {
		elem 		= $(event.relatedTarget);
	  	modalArea 	= $(this);
	  	
	  	var title 	= elem.find('.legend').text();
	  	var items 	= elem.find('img').attr('alt');
	  	items 		= '<p>' + items.replace(/,/g, '</p><p>') + '</p>';

	  	modalArea.find('h3').html(title);
	  	modalArea.find('.boxItems').html(items);
	});

	$('#servicesModal').on('hidden.bs.modal', function (event) {
		modalArea.find('.h3').html('Carregando...');
	  	modalArea.find('.boxItems').html('Carregando...');
	});

	$('#photosModal').on('hidden.bs.modal', function (event) {
	  	modalArea.find('.getImage').html('');
	  	modalArea.find('h4').html('');
	});
	// Arrow of navagate from Gallery
	$('.arrowLeft, .arrowRight').on('click', function(){
		if ($(this).hasClass('arrowRight')) {
			changeImage('right');
		} else {
			changeImage('left');
		}
	});
	/**
	 * End Gallery
	 */
	
	$('.box-content a').on('click', function(event){
		event.preventDefault();
	});

	/**
	 * Valid Form
	 */
	$('#formModal form .btn-valid').on('click', function(event){

		var msg = '';

		if ( ( $('#nome').val() == '' || $('#nome').lenght < 3 ) ) {
			msg += "<p class='alert alert-danger'>O nome é inválido.</p>";
		}
		if ( ( $('#telefone').val() == '' ||  $('#nome').lenght < 3 ) ) {
			msg += "<p class='alert alert-danger'>O telefone é inválido.</p>";
		}
		if ( ( $('#email').val() == '' || $('#nome').lenght < 3 ) || ( ! is_email($('#email').val()) ) ) {
			msg += "<p class='alert alert-danger'>O e-mail é inválido.</p>";
		}
		if ( ( $('#mensagem').val() == '' ||  $('#nome').lenght < 3 ) ) {
			msg += "<p class='alert alert-danger'>É preciso inserir uma mensagem.</p>";
		}

		if (msg == '') {

			var url 	= $('#formModal form').attr('action');
			var data 	= $('#formModal form').serialize();

			ga('send', {
				'hitType': 'event',
				'eventCategory': originToGa,
				'eventAction': 'click',
				'eventLabel': 'Envio de formulário de contato'
			});

			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: data,
				async: false,
				success: function(json){
					console.log(json);
					if (json == true) {
						// Success
						msg += "<p class='alert alert-success'>Enviado com sucesso! Em breve te retornaremos</p>";
						setTimeout(function(){
							$('.modal .close').click();
						}, 5000);

					} else {
						// Error
					}
				},
				error: function(error){
					console.log(error);
				}
			});
		}

		$('.msg').html(msg);
	});

	$('#formModal').on('hidden.bs.modal', function (event) {
		$(this).find('form input').val('');
		$(this).find('form textarea').val('');
		$('.msg').html('');
	});
	/**
	 * END Valid Form
	 */

	$('#menu a').click(function(){
		var el = $(this).attr('href');
		var scrollValue = 0;
		if (el == '#gallery') {scrollValue = $('.gallery').offset().top}
		if (el == '#video') {scrollValue = $('.video').offset().top}
		if (el == '#plan') {scrollValue = $('.plan').offset().top}
		if (el == '#services') {scrollValue = $('.services').offset().top}
		if (el == '#location') {scrollValue = $('.location').offset().top}
		if (el == '#differential') {scrollValue = $('.differential').offset().top}
		if (el == '#idealizers') {scrollValue = $('.idealizers').offset().top - 100}

		scrollPage(scrollValue);

		if ( $('button.navbar-toggle').is(':visible') ) {
			$('.navbar-toggle').click();
		}
	})

	$('.icon-toggle').on('click', function(){

		if ( $(this).hasClass('_show') ) {
			$('.box-message').fadeOut();

			$(this).removeClass('_show');
			$(this).addClass('_hide');
		} else {
			$('.box-message').fadeIn();
			$(this).removeClass('_hide');
			$(this).addClass('_show');
		}
	});

	if ( isMobile() ) {
		$('body').addClass('mobile');
	}

	/**
	 * More images in Gallery
	 */
	$('.more-gallery-link').on('click', function(){
		var img = $(this).find('img');

		if ( img.hasClass('plus') ) {
			img.removeClass('plus').addClass('less');
			$('.more-gallery').slideDown();
			img.attr('src', 'assets/images/less-gallery.png');
		} else {
			img.removeClass('less').addClass('plus');
			$('.more-gallery').slideUp();
			img.attr('src', 'assets/images/more-gallery.png')
		}
	})

	/**
	 * More plan in plans
	 */
	$('.more-plans-link').on('click', function(){
		var img = $(this).find('img');

		if ( img.hasClass('plus') ) {
			img.removeClass('plus').addClass('less');
			$('.more-plans').slideDown();
			img.attr('src', 'assets/images/less-gallery.png');
		} else {
			img.removeClass('less').addClass('plus');
			$('.more-plans').slideUp();
			img.attr('src', 'assets/images/more-gallery.png')
		}
	})

	$('.button-interection').on('click', function(){
		if ( $(this).hasClass('button-chat') ){
			originToGa = 'Chat'; // chat
		} else if( $(this).hasClass('button-call') ) {
			originToGa = 'Form - Ligamos'; // form-ligamos
		} else if( $(this).hasClass('button-msg') ) {
			originToGa = 'Form - Mensagem'; // form-mensagem
		}
	})

	/**
	 * Click in the logo to scroll page
	 */
	var logoHeader = document.querySelector('.logoHeader').onclick = function(event){
		event.preventDefault();
		scrollPage(0);
	};
	var logoFooter = document.querySelector('.logoFooter').onclick = function(event){
		event.preventDefault();
		scrollPage(0);
	};

	/**
	 * Ajsuta box de mensagem
	 */
	adjustBoxMessage();
});

var adjustBoxMessage = function(){
	var wContainer 	= $('.container').width();
	var wBody 		= $('body').width()
	var wReal = wContainer + (( wBody - wContainer) / 2) - 200;

	var hReal = $(window).height() - 211;
	
	$('.box-container-message').css({'left': wReal, 'top': hReal, 'bottom': 'auto'});

}

var scrollPage = function(lenght){
	$('html, body').animate({
        scrollTop: lenght - 100
    }, 1000);
}
var loadGallery = function(el){
	el 		= el.parents('.box');

	el 		= el.find('img');
	var image 	= el.attr('src');
	var title 	= el.attr('alt');
	var num 	= el.attr('data-num');

	if ( image.indexOf('/plan-') != -1) {
  		image 		= image.replace('/images/gallery/plan-min-', '/images/gallery/plan-');
	} else {
  		image 		= image.replace('/images/gallery/image-min-', '/images/gallery/image-');
	}
	
  	image 		= "<img src=" + image + " alt='" + title + "' data-num='" + num + "'>";

  	modalArea.find('.getImage').html(image);
  	modalArea.find('#photosLabel').text(title);
}

/**
 * Alter images with click arrow
 */
var changeImage = function(direction){
	
	var len = sectionPrincipal.find('img.image').length;

	var img = modalArea.find('.getImage img');
	var num = img.attr('data-num');
	var src = img.attr('src');

	if (direction == 'right') {
		num = (num != len) ? ++num : 1;
	} else {
		num = (num != 1) ? --num : len;
	}

	var newAlt = $(sectionPrincipal).find(".box img[src='assets/images/gallery/" + subImgName + '-min-' + num + ".jpg']").attr('alt');
	img.attr('src', 'assets/images/gallery/' + subImgName + '-' + num + '.jpg')
		.attr('data-num', num)
		.attr('alt', newAlt);

	modalArea.find('h4').text(newAlt);
}

function is_email(email){
	er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/; 
	if( !er.exec(email) )
	{
		jQuery('#retorno_capta').html('Email inválido!');
		return false;
	}
	return true;
}

function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}